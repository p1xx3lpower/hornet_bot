from __future__ import nested_scopes
import discord
import json
import urllib


all_games = list()
displayed_games = list()
length = 10
fresh = True

libraries = dict()
indices = dict()
personas = dict()
rows = dict()

client = discord.Client()


def main():
    
    @client.event
    async def on_ready():
        print("hornet_bot ready!")

    # Commands

    @client.event
    async def on_message(message):
        sender = message.author

        if sender == client.user:
            return

        if message.content.startswith('!hello'):
            await message.channel.send('_SHAW!_')

        if message.content == "!easteregg":

            eerole = discord.utils.get(sender.guild.roles, name="Stinker")
            
            if discord.utils.get(sender.roles, name="Stinker"):
                await sender.remove_roles(eerole, atomic=True)
            else:
                await sender.add_roles(eerole, atomic=True)

            await message.delete()

        if message.content.startswith("!recent"):

            id = message.content.split(" ")[1]
            await get_recent(id, message)

        if message.content.startswith("!owned"):

            args = message.content.split(" ")
            id = args[1]

            if ("-rows" in args and len(args) == 4 and int(args[3]) > 0):
                global length
                length = min(int(args[3]), 30)
            else:
                length = 10

            all_games.clear()
            displayed_games.clear()

            await get_owned(id, message)

            await message.delete()

    # Reactions

    @client.event
    async def on_reaction_add(reaction, sender):
        emoji = reaction.emoji
        command = reaction.message
        id = reaction.message.id
        current = indices.get(id)
        print(id)

        global fresh

        if sender == client.user:
            return

        if emoji == '⬇' and reaction.message.author == client.user:
            displayed_games.clear()
            
            fresh = False

            indices.update({id: (current + 1)})
            
            await reaction.remove(sender)
            await display(command)
        
        if emoji == '⬆' and reaction.message.author == client.user:
            displayed_games.clear()
            
            fresh = False

            await reaction.remove(sender)
            if current > 0: 
                indices.update({id: (current - 1)})
                await display(command)

        if emoji == '❌' and reaction.message.author == client.user:
            displayed_games.clear()
            await reaction.message.delete()

    with open('Token.txt') as f:
        token = f.read()

    client.run(token)


async def get_recent(steamid, message):
    ins = str(steamid)
    url = 'https://api.steampowered.com/IPlayerService/GetRecentlyPlayedGames/v1/?key=0A418A492CECB083C36B7902383FD613&steamid={}&count=10'.format(ins)

    # grab recently played games from Steam API
    try:
        x = urllib.request.urlopen(url)

        # parse x:
        y = json.loads(x.read())["response"]["games"]
        y.sort(key = lambda l: l["playtime_2weeks"], reverse=True)

        game_list = "`                      `%5s`        `%4s` /   `%6s` `\n" % ("_Title_", "_Recent_", "_Total_")

        for game in y:
            pt_2w = round(game['playtime_2weeks']/60.0, 1)
            pt_total = round(game['playtime_forever']/60.0, 1)
            if len(game['name']) >= 30: game['name'] = (game['name'][:30] + "...")
            game_list = game_list + "`%32.32s   %4.1fh  / %6.1fh `\n" % (game['name'], pt_2w, pt_total)
        
        await message.channel.send(game_list)
    
    except urllib.error.HTTPError:
        await message.channel.send("_invalid steamid_\n")


async def get_owned(steamid, message):
    ins = str(steamid)
    url = 'https://api.steampowered.com/IPlayerService/GetOwnedGames/v1/?key=0A418A492CECB083C36B7902383FD613&steamid={}&include_appinfo=true'.format(ins)

    # grab recently played games from Steam API
    try:
        x = urllib.request.urlopen(url)

        # parse x:
        y = json.loads(x.read())["response"]["games"]
        y.sort(key = lambda l: l["playtime_forever"], reverse=True)

        for g in y:
            title = g["name"]
            playtime = g["playtime_forever"]

            all_games.append((title, truncate(playtime / 60, 1)))
        
        global fresh
        fresh = True

        # Read profile name
        profile_url = 'https://api.steampowered.com/ISteamUser/GetPlayerSummaries/v2?key=0A418A492CECB083C36B7902383FD613&steamids={}'.format(ins)
        profile = urllib.request.urlopen(profile_url)
        persona = json.loads(profile.read())["response"]["players"][0]["personaname"]
        persona = persona + " - " + ins

        # Add new library to dict
        id = message.id
        lib = all_games.copy()
        libraries.update({id: lib})
        indices.update({id: 0})
        personas.update({id: persona})
        rows.update({id: length})

        print(id)

        await display(message)
    
    except urllib.error.HTTPError:
        await message.channel.send("_invalid steamid_\n")


async def display(command):
    id = command.id
    lib = libraries.get(id)
    page = indices.get(id)
    persona = personas.get(id)

    lim = min(rows[id], len(lib) - page * rows[id])    
    for i in range(lim):
        displayed_games.append(lib[page * rows[id] + i])

    game_list = "`{0: <42}`\n`{1: <42}`\n".format(persona, "")
    game_list = game_list + "`               `{0: <5}`              `{1: <6}` `\n".format("_Title_", "_Playtime_")

    for dg in displayed_games:
        if len(dg[0]) > 30: dg = (dg[0][:30] + "..", dg[1])
        game_list = game_list + "`{0: <32} {1: >6}h  `\n".format(dg[0], dg[1])
    
    reactions = list()

    if fresh:
        # Create new library message
        message = await command.channel.send(game_list)

        # Workaround to sync message id's
        bot_id = message.id
        libraries.pop(id)
        indices.pop(id)
        p = personas.pop(id)
        r = rows.pop(id)

        libraries.update({bot_id: lib})
        indices.update({bot_id: 0})
        personas.update({bot_id: p})
        rows.update({bot_id: r})

        
        reactions.append('❌')    
        if page != 0: reactions.append('⬆')
        if lim == rows[bot_id]: reactions.append('⬇')
    
        for r in reactions:
            await message.add_reaction(r)
    
    else:
        # Edit existing library message / scroll through the library
        await command.edit(content=game_list)
        if page == 0: await command.remove_reaction('⬆', client.user)
        if lim != rows[id]: await command.remove_reaction('⬇', client.user)

        if page != 0: 
            reactions.append('⬆')

        if lim == rows[id]: 
            reactions.append('⬇')

        for r in reactions:
            await command.add_reaction(r)


def truncate(num, n):
    integer = int(num * (10**n))/(10**n)
    return float(integer)


main()